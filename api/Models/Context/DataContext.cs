﻿using api.Models.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models.Context
{
    public class DataContext : DbContext
    {
        public DataContext() { }
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public virtual DbSet<CATEGORY> CATEGORY { get; set; }
        public virtual DbSet<FILE_TYPE> FILE_TYPE { get; set; }
        public virtual DbSet<IDENTITY> IDENTITY { get; set; }
        public virtual DbSet<LANGUAGE> LANGUAGE { get; set; }
        public virtual DbSet<PAGE> PAGE { get; set; }
        public virtual DbSet<PAGE_TYPE> PAGE_TYPE { get; set; }
        public virtual DbSet<PRODUCT> PRODUCT { get; set; }
        public virtual DbSet<PRODUCT_FEATURE_NAME> PRODUCT_FEATURE_NAME { get; set; }
        public virtual DbSet<PRODUCT_FEATURE_NAME_VALUE> PRODUCT_FEATURE_NAME_VALUE { get; set; }
        public virtual DbSet<PRODUCT_FILES> PRODUCT_FILES { get; set; }
        public virtual DbSet<SLIDER> SLIDER { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                           .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                           .AddJsonFile("appsettings.json")
                           .Build();
                optionsBuilder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
            }
        }

        //public virtual DbSet<CATEGORY> CATEGORY { get; set; }
    }
}
