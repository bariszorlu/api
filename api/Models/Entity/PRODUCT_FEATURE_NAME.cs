﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models.Entity
{
    [Table("PRODUCT_FEATURE_NAME")]
    public class PRODUCT_FEATURE_NAME
    {
        [Key]
        public int ID_PRODUCT_FEATURE_NAME { get; set; }

        public int ID_LANGUAGE { get; set; }

        public string NAME { get; set; }

    }
}
