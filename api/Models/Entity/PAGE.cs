﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models.Entity
{
    [Table("PAGE")]
    public class PAGE
    {
        [Key]
        public int ID_PAGE { get; set; }

        public int ID_PAGE_TYPE { get; set; }

        public string PAGE_FILE { get; set; }

        public int FILE_TYPE { get; set; }

        public string DESCRIPTION { get; set; }

        public DateTime CREATEDATE { get; set; }

        public bool ISDELETED { get; set; }

        public bool ISPUBLISH { get; set; }

        public string SLOGAN { get; set; }

        public int ID_LANGUAGE { get; set; }

        public DateTime UPDATEDATE { get; set; }

        public string PAGE_NAME { get; set; }

        public string PAGE_NAME_WEBSITE { get; set; }
        public string BANNER { get; set; }

    }
}
