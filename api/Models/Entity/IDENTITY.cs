﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models.Entity
{
    [Table("IDENTITY")]
    public class IDENTITY
    {
        [Key]
        public int ID_IDENTITY { get; set; }

        public int ID_LANGUAGE { get; set; }

        public string FAVICON { get; set; }

        public string TITLE { get; set; }

        public string DESCRIPTION { get; set; }

        public string SEO_WORDS { get; set; }

    }
}
