﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models.Entity
{
    [Table("CATEGORY")]
    public class CATEGORY
    {
        [Key]
        public int ID_CATEGORY { get; set; }

        public int ID_PARENT { get; set; }

        public string NAME { get; set; }

        public string IMAGE { get; set; }

        public string DESCRIPTION { get; set; }

        public DateTime CREATEDATE { get; set; }

        public bool ISDELETED { get; set; }

        public bool ISPUBLISH { get; set; }

        public string VIDEO { get; set; }

        public string SLOGAN { get; set; }

        public int ID_LANGUAGE { get; set; }

        public DateTime UPDATEDATE { get; set; }
        public string BANNER { get; set; }

    }
}
