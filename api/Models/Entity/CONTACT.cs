﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models.Entity
{
    [Table("CONTACT")]
    public class CONTACT
    {
        [Key]
        public int ID_CONTACT { get; set; }

        public int ID_LANGUAGE { get; set; }

        public string NAME { get; set; }

        public string SLOGAN { get; set; }

        public string PHONE { get; set; }

        public string MAIL { get; set; }

        public string PHONE2 { get; set; }

        public string MAIL2 { get; set; }

        public string ADDRESS { get; set; }

        public string FACEBOOK { get; set; }

        public string TWITTER { get; set; }

        public string LINKEDIN { get; set; }

        public string INSTAGRAM { get; set; }

        public string EXTENALSOCIALMEDIA { get; set; }

        public string EXTENALSOCIALMEDIA2 { get; set; }
        public string BANNER { get; set; }

    }
}
