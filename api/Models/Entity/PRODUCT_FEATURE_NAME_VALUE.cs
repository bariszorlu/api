﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models.Entity
{
    [Table("PRODUCT_FEATURE_NAME_VALUE")]
    public class PRODUCT_FEATURE_NAME_VALUE
    {
        [Key]
        public int ID_PRODUCT_FEATURE_NAME_VALUE { get; set; }

        public int ID_LANUGAGE { get; set; }

        public int ID_PRODUCT_FEATURE_NAME { get; set; }

        public string VALUE { get; set; }

    }
}
