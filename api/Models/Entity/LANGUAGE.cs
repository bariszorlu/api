﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models.Entity
{
    [Table("LANGUAGE")]
    public class LANGUAGE
    {
        [Key]
        public int ID_LANUGAGE { get; set; }

        public string NAME { get; set; }

    }
}
