﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models.Entity
{
    [Table("SLIDER")]
    public class SLIDER
    {
        [Key]
        public int ID_SLIDER { get; set; }

        public int ID_LANGUAGE { get; set; }

        public int ID_PRODUCT { get; set; }

        public string NAME { get; set; }

        public string SLOGAN { get; set; }

        public string SLIDER_FILE { get; set; }

        public int FILE_TYPE { get; set; }

        public bool IS_PUBLISH { get; set; }

        public bool IS_DELETED { get; set; }

        public DateTime CREATEDATE { get; set; }

        public DateTime UPDATEDATE { get; set; }

    }
}
