﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models.Entity
{
    [Table("PRODUCT_FILES")]
    public class PRODUCT_FILES
    {
        [Key]
        public int ID_PRODUCT_FILE { get; set; }

        public int ID_PRODUCT { get; set; }

        public int FILE_TYPE { get; set; }

        public string PRODUCT_FILE { get; set; }

        public int ID_LANGUAGE { get; set; }

        public bool ISPUBLISH { get; set; }

        public DateTime CREATEDATE { get; set; }

        public DateTime UPDATEDATE { get; set; }

        public bool ISDELETED { get; set; }

        public int AREA_TYPE { get; set; }

        public string FILE_NAME { get; set; }

    }

}
