﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models.Entity
{
    [Table("FILE_TYPE")]
    public class FILE_TYPE
    {
        [Key]
        public int ID_FILE_TYPE { get; set; }

        public string NAME { get; set; }
    }
}
