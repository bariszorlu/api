﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models.Entity
{
    [Table("PAGE_TYPE")]
    public class PAGE_TYPE
    {
        [Key]
        public int ID_PAGE_TYPE { get; set; }

        public string NAME { get; set; }

    }
}
